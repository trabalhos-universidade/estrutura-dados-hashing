#include <iostream>
#include <cstdlib>
#include <string>
#include <list>

using namespace std;

const unsigned short int tam = 5; 

class no
{
    private:
        int cod;
        string nome;
        int idade;
    public:
        no();
        no(int cod, string nome, int idade);
        ~no();
        int getcod();
        void setcod(int cod);
        string getnome();
        void setnome(string nome);
        int getid();
        void setid(int id);
        no *getprox();
        void setprox(no *prox);
        void lista();
};

class hash
{
    private:
        list<no> th[5];
    public:
        hash();
        ~hash();
        void insere(no cad);
        void remove(int cod);
        void consulta(int cod, list<no>::iterator &auxhash);
        void lista();
        void consultanome(string nome);
        void edita(int cod);
};


